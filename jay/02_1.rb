# Read input file
input = File.readlines('02.input')

# Filter out any empty lines
input = input.select {|x| x != "\n"}.collect{|x| x.chomp}

doubles = 0
triples = 0

input.each {|line|
  #puts "Processing line #{line}"
  split_line = line.split '' # Get an array of characters
  
  # We only want to count a double/triple once for each line, so we need to remember if one was found
  double_found = false
  triple_found = false

  # For each unique character, see how many of them there are
  split_line.uniq.each {|c|
    #puts "character #{c} appears #{split_line.count(c)} times"
    case split_line.count(c)
    when 2
      double_found = true
    when 3
      triple_found = true
    end
  }

  # And now add to the total doubles/triples
  if double_found
    doubles += 1
  end
  if triple_found
    triples += 1
  end
}

puts "doubles found: #{doubles}"
puts "triples found: #{triples}"
puts "checksum: #{doubles * triples}"
  


## Part 2 - Searching part

# Function to compare two boxes
def compare_boxes(line1, line2)
  # First, zip lines together
  combined = line1.split('').zip(line2.split(''))
  # Then, turn into 0's or 1's if the characters are different
  difference = combined.collect{|x| x[0] != x[1] ? 1 : 0}
  # And return the sum of the difference
  return difference.sum
end

input.each_index {|line_index|
  comparisons = input[(line_index + 1)..-1]
  comparisons.each {|x|
    difference = compare_boxes(input[line_index], x)
    if difference == 1
      puts "difference between #{input[line_index]} and #{x} is #{difference}"
      similar_str = input[line_index].split('').zip(x.split('')).select{|y| y[0] == y[1]}.collect{|y| y[0]}.join
      puts "simlar part of the lines is #{similar_str}"
    end
  }
}


