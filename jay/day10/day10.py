from collections import namedtuple
from dataclasses import dataclass
import cursive_re as cr
from typing import List

# Define a type for a star
@dataclass
class Star:
    pos_x: int
    pos_y: int
    vel_x: int
    vel_y: int

    @classmethod
    def from_str(cls, string: str):
        number = cr.group(cr.maybe(cr.literal('-')) + cr.one_or_more(cr.any_of(cr.in_range('0', '9'))))
        cre = cr.text('position=<') + cr.zero_or_more(' ') \
            + number + cr.text(',') + cr.zero_or_more(' ') \
            + number + cr.text('> velocity=<') + cr.zero_or_more(' ') \
            + number + cr.text(',') + cr.zero_or_more(' ') \
            + number + cr.anything()
        regex = cr.compile(cre)
        vals = map(int, regex.match(string).groups())
        return cls(*vals)


with open('input') as f:
    inp = [Star.from_str(i) for i in f.readlines()]


def step_forward(stars: List[Star], reverse: bool = False):
    if not reverse:
        for s in stars:
            s.pos_x += s.vel_x
            s.pos_y += s.vel_y
    else:
        for s in stars:
            s.pos_x -= s.vel_x
            s.pos_y -= s.vel_y


def stars_len(stars: List[Star]):
    big, small = max(stars, key=lambda star: star.pos_x), min(stars, key=lambda star: star.pos_x)
    return abs(small.pos_x - big.pos_x)

def stars_height(stars: List[Star]):
    big, small = max(stars, key=lambda star: star.pos_y), min(stars, key=lambda star: star.pos_y)
    return abs(small.pos_y - big.pos_y)

old_size = stars_len(inp)
step_forward(inp)
new_size = stars_len(inp)
count = 1
while old_size > new_size:
    # print('stepping forward')
    count += 1
    old_size = new_size
    step_forward(inp)
    new_size = stars_len(inp)

step_forward(inp, reverse=True)

bigx, smallx = max(inp, key=lambda star: star.pos_x).pos_x, min(inp, key=lambda star: star.pos_x).pos_x
bigy, smally = max(inp, key=lambda star: star.pos_y).pos_y, min(inp, key=lambda star: star.pos_y).pos_y


def find_star(stars: List[Star], x: int, y: int):
    for s in stars:
        if s.pos_x == x and s.pos_y == y:
            return s
    return None

for y in range(smally, bigy+1):
    for x in range(smallx, bigx+1):
       if find_star(inp, x, y):
           print('x', end='')
       else:
           print(' ', end='')
    print('')

print(count - 1)




