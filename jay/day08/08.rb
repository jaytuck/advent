input = File.read "input"
input = input.split.collect{|x| x.to_i}

test_in = "2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2".split.collect{|x| x.to_i}

def get_child_size(data)
    return (data[0] + data[1])
end

def get_children(node, num_childs)
    children = []
    pos = 2
    while num_childs > 0
        size = get_child_size(node[pos, 2])
        children << node[pos, size]
        pos += size
        num_childs -= 1
    end
    return children
end

def process_node(node)
    num_childs, num_metadata, data = node[0], node[1], node[2, node.length]

    total_value = 0

    children = Hash.new
    metadata = []
    (1..num_childs).each do |x|
        new_meta, data, value = process_node(data)
        children[x] = value
        metadata.append(new_meta).flatten!
    end
    
    my_meta = data[0, num_metadata]
    children.each do |key, value|
        total_value += (value * my_meta.count(key))
    end

    if num_childs == 0
        return my_meta, data[num_metadata, data.length], my_meta.sum
    else
        metadata.append(data[0, num_metadata]).flatten!
        return metadata, data[num_metadata, data.length], total_value
    end
end

result, _, value = process_node(input)
puts "Result is #{result.sum}. Value is #{value}"
