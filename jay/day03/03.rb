# Read input file
input = File.readlines('03.input')
#
# # Filter out any empty lines
input.select!{|x| x != "\n"}
input.collect!{|x| x.chomp}

class Claim
  def initialize(input_string)
	  input_string
	  id, remainder = input_string[1..-1].split(" @ ")
	  @id = Integer(id)
	  start_pos, size = remainder.split(': ')
	  start_x, start_y = start_pos.split(',')
	  @start_x, @start_y = Integer(start_x), Integer(start_y)
	  size_x, size_y = size.split('x')
	  @size_x, @size_y = Integer(size_x), Integer(size_y)
  end
	attr_reader :id, :start_x, :start_y, :size_x, :size_y	
end

# Map all inputs to claims
input.collect!{|x| Claim.new x}

# Set up some data structures
Spot = Struct.new(:x, :y)
claimed_spots = Hash.new {|h, k| h[k] = 0} 

def cycle_through_claim(claim)
	locations = Array.new
  (1 .. claim.size_x).each do |x_offset|
    (1 .. claim.size_y).each do |y_offset|
			locations.append Spot.new (x_offset + claim.start_x), (y_offset + claim.start_y)
		end
	end
	return locations
end

input.each do |claim|
#	(1 .. claim.size_x).each do |x_offset|
#		(1 .. claim.size_y).each do |y_offset|
#			location = Spot.new (x_offset + claim.start_x), (y_offset + claim.start_y)
	cycle_through_claim(claim).each do |loc|
	 	claimed_spots[loc] += 1
	end
end

multi_claimed_spots = claimed_spots.select{|k, v| v > 1}

puts multi_claimed_spots.length

# Part2
def already_claimed(claim, multi_claimed_spots)
	claimed = false
	cycle_through_claim(claim).each do |location|
		if(multi_claimed_spots.include? location)
			claimed = true
		end
	end
	return claimed
end

input.each do |claim|
	if not already_claimed claim, multi_claimed_spots
		puts "Non-overlapping claim id is #{claim.id}"
	end
end
	


