# Read input file
input = File.readlines('01.input')

# Filter out any empty lines
input = input.select {|x| x != "\n"}

# Turn all input items into integers
input = input.collect {|x| Integer(x)}

puts input.sum
