input = File.readlines "input"

input.collect!{|x| x.chomp.split(', ').collect{|x| x.to_i}}


minx, maxx = input.collect{|i| i[0]}.minmax
miny, maxy = input.collect{|i| i[1]}.minmax

ids = Hash.new
input.each_with_index{|x, i| ids[i] = x}

def get_manhatten_distance(location, target)
    x_dist = (location[0].abs - target[0].abs).abs
    y_dist = (location[1].abs - target[1].abs).abs
    return (x_dist + y_dist)
end

CloseId = Struct.new(:id, :dist)

def get_closest_id(ids, position)
    closests = []
    ids.each do |id, location|
        dist = get_manhatten_distance(location, position)
        current_closest = closests.min_by{|x| x.dist} ? closests.min_by{|x| x.dist}.dist : 99999999
        if dist < current_closest  # new closest
            closests = []
            closests << CloseId.new(id, dist)
        elsif dist == current_closest # distance is the same as previous
            closests << CloseId.new(id, dist)
        end
    end
    return closests
end

location_matrix = Hash.new
((minx-1)..(maxx+1)).each do |x_loc|
    ((miny-1)..(maxy+1)).each do |y_loc|
        closests = get_closest_id(ids, [x_loc, y_loc])
        if closests.length > 1
            location_matrix[[x_loc, y_loc]] = nil
        elsif closests.length == 1
            location_matrix[[x_loc, y_loc]] = closests[0].id
        end
    end
end
   
ids_with_counts = location_matrix.values.group_by{|x| x}.to_h

ids_with_counts = ids_with_counts.collect{|k,v| [k, v.count]}.to_h

infinite_ids = []
((minx-1)..(maxx+1)).each do |x_loc|
    infinite_ids << location_matrix[[x_loc, (miny-1)]]
    infinite_ids << location_matrix[[x_loc, (maxy+1)]]
end
((miny-1)..(maxy+1)).each do |y_loc|
    infinite_ids << location_matrix[[minx-1, y_loc]]
    infinite_ids << location_matrix[[maxx+1, y_loc]]
end

infinite_ids.uniq!
ids_with_counts_fitered = ids_with_counts.select{|k,v| not infinite_ids.include?(k)}

best, best_count = ids_with_counts_fitered.max_by{|k,v| v}

puts "Best location is #{best}. It has #{best_count} free spots around it."

def find_total_distances(location, ids)
    total_distances = 0
    ids.each do |id, id_location|
        total_distances += get_manhatten_distance(location, id_location)
    end
    return total_distances
end

location_matrix_totals = Hash.new

location_matrix.each do |location, _|
    location_matrix_totals[location] = find_total_distances(location, ids)
end

puts "Total places within the 10,000 region are #{location_matrix_totals.select{|k,v| v < 10000}.length}"
